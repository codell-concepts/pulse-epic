var repo = require('../models/repo'),
        _ = require('lodash');

var denormalizer = function(){

    function onInterested(args, callback){
        apply(args, true, callback);
    }

    function onNotInterested(args, callback){
        apply(args, false, callback);
    }

    function apply(args, interested, callback) {
        repo.sessions.findById(args.sessionId, function (err, session) {
            if (err || !session) {
                if (callback){
                    callback(err || new Error('Could not find session ' + args.sessionId));
                }
                return;
            }

            var voter = _.find(session.voters, {userId: args.userId});
            if (voter) {
                voter.interested = interested;
            }
            else {
                session.voters.push({userId: args.userId, interested: interested});
            }
            session.votes = _.where(session.voters, {interested: true}).length;
            session.save(callback);
        });
    }

    function onReset(callback){
        repo.sessions.update({}, {$set: {votes: 0, voters: []}}, {multi: true}, callback || function(){});
    }

    function onSessionAdded(args, callback) {
        repo.sessions.create(args, callback);
    }

    return {
        handleInterested : onInterested,
        handleNotInterested : onNotInterested,
        handleVotesReset : onReset,
        handleSessionAdded: onSessionAdded,
        listenTo : function(emitters){
            emitters = emitters || [];
            if (!_.isArray(emitters))
            {
                emitters = [emitters];
            }
            _.forEach(emitters, function(emitter){
                emitter.on('user-interested', onInterested);
                emitter.on('user-not-interested', onNotInterested);
                emitter.on('votes-reset', onReset);
                emitter.on('session-added', onSessionAdded);
            });
        }
    };

}();

module.exports = denormalizer;
