/* jshint -W097 */
'use strict';

var repo = require('../models/repo'),
  validator = require('node-validator'),
  util = require('util'),
  Emitter = require('events').EventEmitter;

var Sessions = function(){

  var me = this,
      check = validator.isObject().withRequired('title').withRequired('desc');

  Emitter.call(me);

  function validate(args, callback){
    validator.run(check, args, function(errCount){
      callback(errCount > 0 ? new Error('Invalid args, expected {title: x, desc: y}') : null);
    });
  }

  function announce(err, event, obj){
    if (!err){
      me.emit(event, obj);
    }
    return {
      call : function(callback){
        callback(err);
      }
    };
  }

  me.add = function(args, callback){
    validate(args, function(err){
      announce(err, 'session-added', args).call(callback);
    });
  };

};

util.inherits(Sessions, Emitter);
module.exports = new Sessions();
