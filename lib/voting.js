/* jshint -W097 */
'use strict';

var repo = require('../models/repo'),
    validator = require('node-validator'),
    _ = require('lodash'),
    util = require('util'),
    Emitter = require('events').EventEmitter;

var Voting = function(){
    var me = this,
        check = validator.isObject().withRequired('sessionId').withRequired('userId');

    Emitter.call(me);

    function process(args, interested, callback){
        validate(args, function(err){
            if (err){
                callback(err);
                return;
            }
            apply(args, interested, callback);
        });
    }

    function validate(args, callback){
        validator.run(check, args, function(errCount){
            callback(errCount > 0 ? new Error('Invalid args, expected {sessionId: x, userId: y}') : null);
        });
    }

    function apply(args, interested, callback){
        repo.votes.findOne(args, function (err, vote) {
            if (err) {
                callback(err);
                return;
            }
            if (!vote) {
                create(args, interested, callback);
            }
            else {
                update(vote, args, interested, callback);
            }
        });
    }

    function create(args, interested, callback){
        repo.votes.create(_.extend({interested: interested}, args), function(err){
            announce(err, resolve(interested), args).call(callback);
        });
    }

    function update(vote, args, interested, callback) {
        if (vote.interested !== interested) {
            vote.interested = interested;
            vote.save(function (err) {
                announce(err, resolve(interested), args).call(callback);
            });
        }
        else {
            callback();
        }
    }

    function resolve(interested){
        return interested ? 'user-interested' : 'user-not-interested';
    }

    function reset(callback){
        repo.votes.remove({}, function(err){
            announce(err, 'votes-reset').call(callback);
        });
    }

    function announce(err, event, obj){
        if (!err){
            me.emit(event, obj);
        }
        return {
            call : function(callback){
                callback(err);
            }
        };
    }

    me.interested = function(args, callback){
        process(args, true, callback);
    };

    me.notInterested = function(args, callback){
        process(args, false, callback);
    };

    me.reset = reset;
};

util.inherits(Voting, Emitter);
module.exports = new Voting();
