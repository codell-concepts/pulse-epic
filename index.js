var voting = require('./lib/voting'),
    sessions = require('./lib/sessions'),
    repo = require('./models/repo'),
    denormalizer = require('./lib/session.denormalizer');

var epic = function(){
    denormalizer.listenTo([voting, sessions]);
    return {
        sessions: function(callback){
            repo.sessions.where('hide').ne(true).sort('display').lean().exec(callback);
        },
        allSessions: function(callback){
            repo.sessions.find({}).sort('display').lean().exec(callback);
        },
        addSession: function(args, callback){
            sessions.add(args, callback);
        },
        interested: function(args, callback){
            voting.interested(args, callback);
        },
        notInterested: function(args, callback){
            voting.notInterested(args, callback);
        },
        reset: voting.reset
    };
}();

module.exports = epic;

