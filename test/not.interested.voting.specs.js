'use strict';

var setup = require('./setup'),
    sut = require('../lib/voting'),
    repo = require('../models/repo');

describe('when a member is not interested in a session', function(){

   describe('and has not voted on the session yet', function(){

       var args = setup.create.args(),
           announced = false;

       before(function(done){
           sut.on('user-not-interested', function(obj){
              announced = obj === args;
           });
           sut.notInterested(args, done);
       });

       it ('should save the vote for the session', function(done){
            repo.votes.findOne(args, function(err, vote){
                vote.should.exist;
                vote.interested.should.be.false;
                done();
            });
       });

       it('should announce the user is not interested', function () {
            announced.should.be.true;
       });

       after(function(done){
           repo.votes.remove(args, done);
       })
   });

    describe('and has already voted as not being interested', function () {

        var args = setup.create.args(),
            announced = false;

        before(function(done){
            sut.notInterested(args, function(){
                sut.on('user-not-interested', function(obj){
                    announced = obj === args;
                });
                sut.notInterested(args, done);
            });
        });

        it('should not double count the vote', function (done) {
            repo.votes.count(args, function(err, count){
                count.should.equal(1);
                done();
            });
        });

        it('should not announce the user is interested', function () {
            announced.should.be.false;
        });

        after(function(done){
           repo.votes.remove(args, done);
        });
    });

    describe('and has already voted as being interested', function () {

        var args = setup.create.args(),
            announced = false;

        before(function(done){
            sut.interested(args, function(){
                sut.on('user-not-interested', function(obj){
                    announced = obj == args;
                });
                sut.notInterested(args, done);
            });
        });

        it('should not have multiple votes for the same user and session', function (done) {
            repo.votes.count(args, function(err, count){
                count.should.equal(1);
                done();
            });
        });

        it('should update the user as not being interested in the session', function (done) {
            repo.votes.findOne(args, function(err, vote){
                vote.should.exist;
                vote.interested.should.be.false;
                done();
            });
        });

        it('should announce the user is not interested', function () {
            announced.should.be.true;
        });

        after(function(done){
           repo.votes.remove(args, done);
        });
    });

    describe('without providing a session id', function () {
        it('should indicate the arguments are invalid', function (done) {
            sut.notInterested({userId: 'xxx'}, function(err){
                expect(err).to.exist;
                done();
            });
        });
    });

    describe('without providing a user id', function () {
        it('should indicate the arguments are invalid', function (done) {
            sut.notInterested({sessionId: 'xxx'}, function(err){
                expect(err).to.exist;
                done();
            });
        });
    });
});
