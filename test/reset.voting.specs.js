'use strict';
var setup = require('./setup'),
    repo = require('../models/repo'),
    sut = require('../lib/voting');

describe('when resetting votes', function () {

    var announced = false;

    before(function (done) {
        sut.interested(setup.create.args(), function(){
            sut.on('votes-reset', function(){
                announced = true;
            });
            sut.reset(function(){
                done();
            });
        });

    });

    it('should clear all votes', function(done) {
        repo.votes.count({}, function(err, count){
           count.should.equal(0);
            done();
        });
    });

    it('should announce that votes were reset', function () {
        announced.should.be.true;
    });
});
