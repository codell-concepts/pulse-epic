'use strict';
var setup = require('./setup'),
    sut = require('../lib/session.denormalizer'),
    repo = require('../models/repo');

describe('when a session has interest from a user', function () {
    describe('who has never voted for the session', function () {
        var doc = setup.create.sessionDoc(),
            args = null,
            session = null;

        before(function(done) {
            repo.sessions.create(doc, function(err, created){
                args = setup.create.args(created);
                sut.handleInterested(args, function(err, handled){
                    session = handled.toJSON();
                    done();
                });
            });
        });

        it('should increment the vote count by one', function () {
            session.votes.should.equal(doc.votes + 1);
        });

        it('should add the interested user', function () {
            session.voters.should.contain(setup.create.interestedVoter(args.userId));
        });

        after(function(done){
            repo.sessions.findByIdAndRemove(args.sessionId, done);
        })
    });

    describe('who was previously not interested in the session', function () {
        var voter = setup.create.notInterestedVoter(),
            doc = setup.create.sessionDoc().with(voter),
            args = null,
            session = null;

        before(function(done) {
            repo.sessions.create(doc, function(err, created){
                args = setup.create.args(created, voter);
                sut.handleInterested(args, function(err, handled){
                    session = handled.toJSON();
                    done();
                });
            });
        });

        it('should increment the vote count by one', function () {
            session.votes.should.equal(doc.votes + 1);
        });

        it('should update the interested user', function () {
            session.voters.should.contain(setup.create.interestedVoter(args.userId));
        });

        it('should not have more than one vote for the user', function () {
            session.voters.length.should.equal(doc.voters.length);
        });

        after(function(done){
            repo.sessions.findByIdAndRemove(args.sessionId, done);
        })
    });
});
