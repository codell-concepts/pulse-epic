/* jshint -W097 */
'use strict';

var setup = require('./setup'),
    sut = require('../index'),
    repo = require('../models/repo'),
    _ = require('lodash');

describe('when getting sessions', function () {

    var desc = 'delete me',
        first = setup.create.sessionDoc().displayed(1).description(desc),
        second = setup.create.sessionDoc().displayed(5).description(desc),
        hidden = setup.create.sessionDoc().hidden().description(desc),
        docs = [second, hidden, first],
        sessions = [];


    before(function(done){
        repo.sessions.remove({}, function(){
            repo.sessions.create(docs, function(){
                sut.sessions(function(err, found){
                    sessions = found;
                    done();
                });
            });
        });
    });

    it("should order by display", function () {
        sessions[0].display.should.equal(first.display);
        sessions[1].display.should.equal(second.display);
    });

    it("should not return hidden sessions", function () {
        _.where(sessions, {hide: true}).length.should.equal(0);
    });


    after(function(done){
        repo.sessions.remove({desc: desc}, done);
    })
});


