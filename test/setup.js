var chai = require('chai'),
    random = require('randomstring'),
    config = require('config'),
    _ = require('lodash');

global.should = chai.should();
global.expect = chai.expect;

var SessionDoc = function(){
    var me = this;
    me.title = random.generate();
    me.votes = 0;
    me.voters = [];
    me.with = function(voter){
       me.voters.push(voter);
       me.votes = _.where(me.voters, {interested: true}).length;
       return me;
    };
    me.displayed = function(val){
        me.display = val;
        return me;
    };
    me.description = function(value){
        me.desc = value;
        return me;
    };
    me.hidden = function(){
        me.hide = true;
      return me;
    };
    return me;
};

var setup = function(){
    return {
        config : config,
        create : {
            args : function(session, voter){
                return {sessionId: (session ? session.id: random.generate()), userId: (voter ? voter.userId: random.generate())};
            },
            sessionDoc : function(){
                return new SessionDoc();
            },
            interestedVoter : function(uid){
                return {userId: uid || random.generate(), interested: true};
            },
            notInterestedVoter : function(uid){
              return {userId: uid || random.generate(), interested: false};
            }
        }
    }
}();

module.exports = setup;


