'use strict';
var setup = require('./setup'),
    sut = require('../lib/session.denormalizer'),
    repo = require('../models/repo');

describe('when session votes have been reset', function () {
    describe('who has never voted for the session', function () {
        var doc = setup.create.sessionDoc().with(setup.create.interestedVoter()),
            args = null,
            session = null;

        before(function(done) {
            repo.sessions.create(doc, function(err, created){
                args = setup.create.args(created);
                sut.handleVotesReset(function(){
                    repo.sessions.findByIdAndRemove(args.sessionId, function(err, found){
                        session = found.toJSON();
                        done();
                    })
                });
            });
        });
        

        it('sessions should have a count of zero', function () {
            session.votes.should.equal(0);
        });

        it('sessions should not have any interested users', function () {
            session.voters.should.have.length(0);
        });

        after(function(done){
            repo.sessions.findByIdAndRemove(args.sessionId, done);
        })
    });
});
