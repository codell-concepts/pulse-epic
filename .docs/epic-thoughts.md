* just got things working...seemed hacky...scattered logic...scattered validation
* easy to get something going
* trying to look at it from a larger application viewpoint...not just CRUD
* needed structure / pattern / but what?
* **microservices**
* modularity
* domain in a different view
* event emitter
* readmodels
* leaky abstraction with database
* repo
* unit testing and mocking out database - didn't
* mocha and 'done'
* asynchronous nailed me on interested voting specs!!!!  done() called multiple times
* testing affecting my design (exposed public methods on session.denormalizer, injected in voting).  Is this a symptom/challenge of testing evented systems?
* asserting element in array and mongoose...trick / lean for simple comparison.  See denormalizer.

**Continuous integration**
* Codeship / mongo tests just ran
* tests just ran, spun up vm with mongo installed!
* npm install
* npm test
* Codeship notifications Email / HipChat / Slack / Campfire / Grove etc...

