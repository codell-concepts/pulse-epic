/* jshint -W097 */
'use strict';

var config = require('config'),
    mongoose = require('mongoose'),
    votes = require('./vote'),
    session = require('./session');

var repo = function(){
    mongoose.set('debug', config.debug);
    mongoose.connect(process.env.DB || config.db);
    mongoose.connection.on('error', console.error.bind(console, 'failed to connect to db...'));
    process.on('exit', function(){
        mongoose.disconnect();
    });
    return {
        votes: votes,
        sessions: session
    };
}();

module.exports = repo;
