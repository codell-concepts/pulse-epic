var mongoose = require('mongoose');
var schema = new mongoose.Schema({
    title: String,
    display: Number,
    desc: String,
    color: String,
    votes: Number,
    hide: Boolean,
    voters: [new mongoose.Schema({userId: String, interested: Boolean}, {_id: false})]});
module.exports = mongoose.model('Session', schema, 'Sessions');