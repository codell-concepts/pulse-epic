var mongoose = require('mongoose');
var schema = new mongoose.Schema({
    userId: String,
    sessionId: String,
    interested: Boolean});
module.exports = mongoose.model('SessionVote', schema, 'SessionVotes');
